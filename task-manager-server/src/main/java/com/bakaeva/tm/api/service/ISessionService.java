package com.bakaeva.tm.api.service;

import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.dto.SessionDTO;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    void close(@NotNull final SessionDTO session);

    void closeAll(@NotNull final SessionDTO session);

    @Nullable
    UserDTO getUser(@NotNull final SessionDTO session);

    @NotNull
    String getUserId(@NotNull final SessionDTO session);

    @NotNull
    List<SessionDTO> findAll(@NotNull SessionDTO session);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    boolean isValid(@NotNull final SessionDTO session);

    void validate(@NotNull final SessionDTO session);

    void validate(@NotNull final SessionDTO session, @Nullable Role role);

    @Nullable
    SessionDTO open(@NotNull final String login, @NotNull final String password);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void signOutByUserId(@Nullable final String userId);


}