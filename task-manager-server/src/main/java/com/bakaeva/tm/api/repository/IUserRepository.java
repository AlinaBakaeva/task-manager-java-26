package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

}