package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDTO> {

    void clear(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findByName(@NotNull String userId, @NotNull String name);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @NotNull String name);

}