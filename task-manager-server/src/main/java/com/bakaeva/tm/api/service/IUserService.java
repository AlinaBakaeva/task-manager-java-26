package com.bakaeva.tm.api.service;

import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.dto.UserDTO;
import com.bakaeva.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<UserDTO> {

    void create(@Nullable String login, @Nullable String password);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void updateById(
            @Nullable String id, String login,
            @Nullable String firstName, @Nullable String lastName, @Nullable String middleName,
            @Nullable String email
    );

    void updatePasswordById(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @NotNull
    IUserRepository getRepository();

}