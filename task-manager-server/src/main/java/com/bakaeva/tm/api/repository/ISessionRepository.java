package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDTO> {

    List<SessionDTO> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

    @Nullable
    SessionDTO findById(@NotNull final String id);

}