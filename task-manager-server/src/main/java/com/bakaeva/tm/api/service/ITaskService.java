package com.bakaeva.tm.api.service;

import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name
    );

    void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            @Nullable final String description
    );

    void add(@Nullable String userId, @Nullable TaskDTO task);

    void remove(@Nullable String userId, @Nullable TaskDTO task);

    void clear(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ITaskRepository getRepository();

}