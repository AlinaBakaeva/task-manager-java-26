package com.bakaeva.tm.api;

import com.bakaeva.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E extends AbstractEntityDTO> {

    List<E> findAll();

    void clear();

    void load(@Nullable List<E> list);

    void remove(@Nullable E entity);

    void merge(@NotNull E entity);

    void persist(@NotNull E entity);

}