package com.bakaeva.tm.exception.empty;

public final class EmptyLastNameException extends RuntimeException  {

    public EmptyLastNameException() {
        super("Error! Last Name is empty...");
    }

}