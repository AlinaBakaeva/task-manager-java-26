package com.bakaeva.tm.exception.system;

public final class NoSuchPropertyException extends RuntimeException {

    public NoSuchPropertyException(String value) {
        super("Error! No such property in property file: ``" + value + "``...");
    }

}