package com.bakaeva.tm.exception.empty;

public final class EmptyFirstNameException extends RuntimeException {

    public EmptyFirstNameException() {
        super("Error! First Name is empty...");
    }

}