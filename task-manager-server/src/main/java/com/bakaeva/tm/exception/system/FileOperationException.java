package com.bakaeva.tm.exception.system;

public final class FileOperationException extends RuntimeException {

    public FileOperationException(String value) {
        super("File Access Error! ``" + value + "``...");
    }

}