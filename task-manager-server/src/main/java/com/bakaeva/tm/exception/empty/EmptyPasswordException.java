package com.bakaeva.tm.exception.empty;

public final class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}