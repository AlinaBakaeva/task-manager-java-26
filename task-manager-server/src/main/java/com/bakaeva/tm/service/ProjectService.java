package com.bakaeva.tm.service;

import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.api.service.IProjectService;
import com.bakaeva.tm.dto.ProjectDTO;
import com.bakaeva.tm.exception.empty.EmptyDescriptionException;
import com.bakaeva.tm.exception.empty.EmptyIdException;
import com.bakaeva.tm.exception.empty.EmptyNameException;
import com.bakaeva.tm.exception.empty.EmptyUserIdException;
import com.bakaeva.tm.exception.incorrect.IncorrectIdException;
import com.bakaeva.tm.exception.incorrect.IncorrectIndexException;
import com.bakaeva.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectService extends AbstractService<ProjectDTO, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public IProjectRepository getRepository() {
        @NotNull final EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(em);
        return repository;
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        persist(project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        persist(project);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyUserIdException();
        projectDTO.setUserId(userId);
        persist(projectDTO);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) return;
        projectDTO.setUserId(userId);
        remove(projectDTO);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final List<ProjectDTO> result = repository.findAll(userId);
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.clear(userId);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository repository = getRepository();
        @Nullable final ProjectDTO result = repository.findById(userId, id);
        return result;
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository repository = getRepository();
        @Nullable final ProjectDTO result = repository.findByIndex(userId, index);
        return result;
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = getRepository();
        @Nullable final ProjectDTO result = repository.findByName(userId, name);
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeById(userId, id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeByIndex(userId, index);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeByName(userId, name);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) throw new IncorrectIdException();
        project.setName(name);
        project.setDescription(description);
        merge(project);
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) throw new IncorrectIndexException();
        project.setName(name);
        project.setDescription(description);
        merge(project);
    }

}