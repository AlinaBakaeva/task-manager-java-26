package com.bakaeva.tm.service;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.api.IService;
import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.dto.AbstractEntityDTO;
import com.bakaeva.tm.exception.incorrect.IncorrectDataFileException;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntityDTO, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected IServiceLocator serviceLocator;

    @Override
    public @NotNull List<E> findAll() {
        @NotNull final R repository = getRepository();
        @NotNull final List<E> result = repository.findAll();
        return result;
    }

    @Override
    public void clear() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.clear();
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void load(@Nullable final List<E> list) {
        if (list == null) throw new IncorrectDataFileException();
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.load(list);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.remove(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    public void merge(@NotNull E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.merge(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    public void persist(@NotNull E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.persist(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    public abstract R getRepository();

}