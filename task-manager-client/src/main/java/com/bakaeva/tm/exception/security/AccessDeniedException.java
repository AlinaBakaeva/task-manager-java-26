package com.bakaeva.tm.exception.security;

public final class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}