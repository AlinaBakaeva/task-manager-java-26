package com.bakaeva.tm.util;

import com.bakaeva.tm.exception.system.NotNumberException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e){
            throw new NotNumberException(value);
        }

    }

}