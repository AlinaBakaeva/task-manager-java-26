package com.bakaeva.tm.command.data.json;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.DomainEndpoint;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA JSON LOAD]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.loadFromJson(session)) System.out.println("[OK]");
    }

}