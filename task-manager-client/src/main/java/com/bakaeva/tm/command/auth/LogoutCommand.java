package com.bakaeva.tm.command.auth;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.setCurrentSession(null);
        System.out.println("[OK:]");
    }

}