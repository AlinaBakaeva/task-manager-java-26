package com.bakaeva.tm.command.data.xml;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.DomainEndpoint;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove xml data file.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA XML CLEAR]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.removeXml(session)) System.out.println("[OK]");
    }


}