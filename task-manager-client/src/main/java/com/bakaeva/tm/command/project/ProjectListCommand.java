package com.bakaeva.tm.command.project;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.ProjectDTO;
import com.bakaeva.tm.endpoint.SessionDTO;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<ProjectDTO> projects = endpointLocator.getProjectEndpoint().findProjectAll(session);
        for (@Nullable final ProjectDTO project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}